#!/bin/bash

if ! [ "$(id -u)" = 0 ];then
             echo "You are not root, run this target as root please"
             exit 1
fi

cp ./sync.service /etc/systemd/system/
cp ./sync /usr/bin
systemctl daemon-reload