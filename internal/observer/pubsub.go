package observer

// Описание подписчика
type Subscriber interface {
	Notify([]string)
	Close()
}

// Описание издателя
type Publisher interface {
	//start()                              // Запуск работы издателя
	AddSubscriber() chan<- Subscriber    // Дабавление подписчика
	RemoveSubscriber() chan<- Subscriber // Удаление подписчика
	PublishMessage() chan<- interface{}  // Публикация сообщения
	Stop()                               // Остановить работу издателя
}
