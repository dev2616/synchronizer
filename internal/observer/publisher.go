package observer

import (
	"context"
	"sync"
	"time"

	"gitlab.rebrainme.com/golang_users_repos/2240/synchronizer/internal/reader"
)

var wg sync.WaitGroup

// Структура реализации издателя
type publisher struct {
	subscribers []Subscriber     // Список подписчиков
	addSubCh    chan Subscriber  // Канал для добавления подписчика
	removeSubCH chan Subscriber  // Канал для удаление подписчика
	inMsg       chan interface{} // Канал входящего сообщения
	stop        chan struct{}    // Казал для закрытия всех соединений
}

// Медот добавления подписчика
// Метод возвращает канал для записи в него нового подписчика
func (p *publisher) AddSubscriber() chan<- Subscriber {
	return p.addSubCh
}

// Метод для удаления подписчика
// Метод возвращает канал для записи в него подписчика подлежащего удалению
func (p *publisher) RemoveSubscriber() chan<- Subscriber {
	return p.removeSubCH
}

// Метод публикации сообщения
// Метод возвращает канал для записи входящего сообщения
func (p *publisher) PublishMessage() chan<- interface{} {
	return p.inMsg
}

// Метод для закрытия соединения
func (p *publisher) Stop() {
	p.stop <- struct{}{}
}

// Метод запуска издателя
func (p *publisher) Start(ctx context.Context, wg *sync.WaitGroup, srcDir string) {
	tick := time.NewTicker(time.Second * 4)
	for {
		select {
		// Получаем нового подписчика из канала
		case sub := <-p.addSubCh:
			// Добавляем полученного подписчика к остальным
			p.subscribers = append(p.subscribers, sub)
		// Получаем подписчика из канала для удаления
		case sub := <-p.removeSubCH:
			// Перебираем всех подписчиков
			for i, s := range p.subscribers {
				// Елси подписчик полученный из канала существует
				if sub == s {
					// Удаляем его из списка подписчиков
					p.subscribers = append(p.subscribers[:i], p.subscribers[i+1:]...)
					// закрываем канал подписчика
					s.Close()
					break
				}
			}
		case <-ctx.Done():
			wg.Done()
			return
		case <-tick.C:
			fl, _ := reader.ListDir(srcDir)
			for _, sub := range p.subscribers {
				sub.Notify(fl)
			}
		}
	}
}

func (p *publisher) GetSubscribers() []Subscriber {
	return p.subscribers
}

// Создание нового издателя
func NewPublisher() *publisher {
	em := publisher{
		addSubCh:    make(chan Subscriber),
		removeSubCH: make(chan Subscriber),
		inMsg:       make(chan interface{}),
		stop:        make(chan struct{}),
	}
	return &em
}
