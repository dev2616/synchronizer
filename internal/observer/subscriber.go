package observer

import (
	"fmt"

	"gitlab.rebrainme.com/golang_users_repos/2240/synchronizer/internal/equal"
	"gitlab.rebrainme.com/golang_users_repos/2240/synchronizer/internal/reader"
	"gitlab.rebrainme.com/golang_users_repos/2240/synchronizer/internal/syncdir"
)

// Структура реализации подписчика
type subscriber struct {
	in   chan []string // Канал для входящего сообщщения
	stop chan struct{} // Канал для остановки
}

// Метод получения сообщения
func (s subscriber) Notify(msg []string) {
	s.in <- msg
}

// Метод закрытия подписчика
func (s subscriber) Close() {
	close(s.stop)
}

// Метод запуска подписчика
func (s subscriber) start(sourceDir, destDir string) {
	for {
		select {
		// Получаем сообщение из канала
		case msg := <-s.in:
			// Читаем сообщение
			//test := []string{"wefwefwef", "wef2332"}
			fl, _ := reader.ListDir(destDir)
			r, err := equal.Equal(msg, fl)
			if err != nil {
				fmt.Println(err)
			}

			if !r {
				syncdir.SyncDirectory(sourceDir, destDir)
			}

		// закрываем соединеие
		case <-s.stop:
			close(s.in)
			return
		}
	}
}

// Создание нового подписчика
func NewSubscriber(srcDir, destDir string) subscriber {
	sub := subscriber{
		in:   make(chan []string),
		stop: make(chan struct{}),
	}
	go sub.start(srcDir, destDir)
	return sub
}
