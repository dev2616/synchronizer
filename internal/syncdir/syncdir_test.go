package syncdir

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
)

func BenchmarkSyncDirectory(b *testing.B) {
	srcDir := "/home/opus/test_sync"
	destDir := "/home/opus/test_sync_sub"
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		SyncDirectory(srcDir, destDir)
	}
}

func BenchmarkCopy(b *testing.B) {
	srcFile := "./test_dir/test.txt"
	destFile := "./test_dir/test10.txt"
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		copy(srcFile, destFile)
	}
}

func TestSyncDirectory(t *testing.T) {
	req := require.New(t)
	tests := map[string]struct {
		firstVal  string
		secondVal string
		want      error
	}{
		"equalValue": {
			firstVal:  "../../test/test_dir",
			secondVal: "../../test/test_dir_sub/",
			want:      nil,
		},
		"eptySrcValue": {
			firstVal:  "",
			secondVal: "../../test/test_dir_sub/",
			want:      errors.New("No such file or directory"),
		},
		"eptyDestValue": {
			firstVal:  "../../test/test_dir",
			secondVal: "",
			want:      errors.New("No such file or directory"),
		},
	}
	for name, testCase := range tests {
		t.Run(name, func(t *testing.T) {
			err := SyncDirectory(testCase.firstVal, testCase.secondVal)

			req.Equal(testCase.want, err)
		})
	}
}

func TestRemoveFiles(t *testing.T) {
	req := require.New(t)
	tests := map[string]struct {
		firstVal  string
		secondVal string
		want      error
	}{
		"equalValue": {
			firstVal:  "../../test/test_dir",
			secondVal: "../../test/test_dir_sub/",
			want:      nil,
		},
		"eptyValue": {
			firstVal:  "../../test/test_dir",
			secondVal: "",
			want:      errors.New("No such file or directory"),
		},
	}
	for name, testCase := range tests {
		t.Run(name, func(t *testing.T) {
			err := removeFiles(testCase.firstVal, testCase.secondVal)

			req.Equal(testCase.want, err)
		})
	}
}
