package syncdir

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"syscall"

	"gitlab.rebrainme.com/golang_users_repos/2240/synchronizer/internal/loger"
)

var errE = errors.New("File exiest")

// Функция синхронизации дерикторий
func SyncDirectory(scrDir, dest string) error {
	//removeFileList := []string{}
	a := loger.NewLogChanel()
	// Читаем исходную директорию
	srcFiles, err := ioutil.ReadDir(scrDir)
	if _, ok := err.(*os.PathError); ok {
		return errors.New("No such file or directory")
	}

	for _, file := range srcFiles {
		sourcePath := filepath.Join(scrDir, file.Name())
		destPath := filepath.Join(dest, file.Name())

		dstFiles, err := ioutil.ReadDir(dest)
		if _, ok := err.(*os.PathError); ok {
			return errors.New("No such file or directory")
		}
		for _, dstFile := range dstFiles {
			dp := filepath.Join(dest, dstFile.Name())
			sp := filepath.Join(scrDir, dstFile.Name())
			if exists(dp) && !exists(sp) {
				err := os.RemoveAll(dp)
				if err != nil {
					return err
				}
				fileInfo, err := os.Stat(dp)
				a <- fmt.Sprintf("File Name : {%s} Operation Type : {%s} Size Size : %d\n", dp, "DELETE", fileInfo.Size())
			}
		}

		fileInfo, err := os.Stat(sourcePath)
		if err != nil {
			return err
		}

		stat, ok := fileInfo.Sys().(*syscall.Stat_t)
		if !ok {
			return fmt.Errorf("failed to get raw syscall.Stat_t data for '%s'", sourcePath)
		}

		switch fileInfo.Mode() & os.ModeType {
		case os.ModeDir:
			if err := createIfNotExists(destPath, 0755); err != nil {
				return err
			}
			if err := SyncDirectory(sourcePath, destPath); err != nil {
				return err
			}
		case os.ModeSymlink:
			if err := copySymLink(sourcePath, destPath); err != nil {
				return err
			}
		default:
			if err := copy(sourcePath, destPath); err != nil {
				return err
			}
		}
		if err := os.Lchown(destPath, int(stat.Uid), int(stat.Gid)); err != nil {
			return err
		}
		isSymlink := file.Mode()&os.ModeSymlink != 0
		if !isSymlink {
			if err := os.Chmod(destPath, file.Mode()); err != nil {
				return err
			}
		}
	}
	return nil
}

func removeFiles(srcDir, destDir string) error {
	dstFiles, err := ioutil.ReadDir(destDir)
	if _, ok := err.(*os.PathError); ok {
		return errors.New("No such file or directory")
	}
	for _, dstFile := range dstFiles {
		dp := filepath.Join(destDir, dstFile.Name())
		sp := filepath.Join(srcDir, dstFile.Name())
		if exists(dp) && !exists(sp) {
			os.RemoveAll(dp)
		}
	}

	return nil
}

func copy(srcFile, dstFile string) error {
	a := loger.NewLogChanel()
	if !exists(dstFile) {
		out, err := os.Create(dstFile)
		if err != nil {
			return err
		}

		defer out.Close()

		in, err := os.Open(srcFile)
		if err != nil {
			return err
		}
		_, err = io.Copy(out, in)
		if err != nil {
			return err
		}
		fileInfo, err := os.Stat(dstFile)
		a <- fmt.Sprintf("File Name : {%s} Operation Type : {%s} Size Size : %d\n", dstFile, "CREATE", fileInfo.Size())
	}
	return nil
}

func exists(filePath string) bool {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return false
	}
	return true
}

func createIfNotExists(dir string, perm os.FileMode) error {
	a := loger.NewLogChanel()
	if exists(dir) {
		return nil
	}
	if err := os.MkdirAll(dir, perm); err != nil {
		return fmt.Errorf("failed to create directory: '%s', error: '%s'", dir, err.Error())
	}
	fileInfo, _ := os.Stat(dir)
	a <- fmt.Sprintf("File Name : {%s} Operation Type : {%s} Size Size : %d\n", dir, "CREATE", fileInfo.Size())
	return nil
}

func copySymLink(source, dest string) error {
	a := loger.NewLogChanel()
	link, err := os.Readlink(source)
	if err != nil {
		return err
	}
	a <- link
	return os.Symlink(link, dest)
}
