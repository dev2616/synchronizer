package reader

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListDir(t *testing.T) {
	req := require.New(t)
	tests := map[string]struct {
		val  string
		want []string
	}{
		"validValue": {
			val:  "./test_dir",
			want: []string{"test_dir/test.txt", "test_dir/test_sub_dir", "test_dir/test_sub_dir", "test_dir/test_sub_dir/test1.txt"},
		},
		"eptyValue": {
			val:  "",
			want: nil,
		},
		"eptyDir": {
			val:  "/home/opus/test_empty_dir",
			want: nil,
		},
		"invalidValie": {
			val:  "tral$sivali",
			want: nil,
		},
	}

	for name, testCase := range tests {
		t.Run(name, func(t *testing.T) {
			res, err := ListDir(testCase.val)
			if err != nil {
				req.EqualError(err, os.ErrNotExist.Error())
			}
			req.Equal(testCase.want, res)
		})
	}
}
