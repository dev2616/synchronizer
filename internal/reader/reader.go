package reader

import (
	"io/fs"
	"os"
	"path/filepath"
)

func ListDir(path string) ([]string, error) {
	var listFiles []string
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, os.ErrNotExist
	}

	filepath.Walk(path, func(wPath string, info fs.FileInfo, err error) error {
		if wPath == path {
			return nil
		}
		if info.IsDir() {
			listFiles = append(listFiles, wPath)
			ListDir(wPath)
			//return filepath.SkipDir
		}

		if wPath != path {
			listFiles = append(listFiles, wPath)
		}
		return nil
	})
	return listFiles, nil
}
