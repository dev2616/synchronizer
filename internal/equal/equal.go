package equal

import (
	"errors"
	"strings"
)

func Equal(a, b []string) (bool, error) {
	if len(a) != len(b) {
		return false, nil
	}

	for i, v := range a {
		if !strings.HasPrefix(v, "/") {
			return false, errors.New("invalid Value")
		}
		as := strings.Split(v, "/")
		bs := strings.Split(b[i], "/")

		if as[len(as)-1:][0] != bs[len(bs)-1:][0] {
			return false, nil
		}
	}
	return true, nil
}
