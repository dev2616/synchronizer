package equal

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEqual(t *testing.T) {
	req := require.New(t)
	tests := map[string]struct {
		firstVal  []string
		secondVal []string
		want      bool
	}{
		"equalValue": {
			firstVal:  []string{"/home/opus/testdir1/", "/home/opus/testdir1/test.txt"},
			secondVal: []string{"/home/opus/testdir2/", "/home/opus/testdir2/test.txt"},
			want:      true,
		},
		"notEqualValue": {
			firstVal:  []string{"/home/opus/testdir1/", "/home/opus/testdir1/test.txt", "/home/opus/testdir1/test10.txt"},
			secondVal: []string{"/home/opus/testdir2/", "/home/opus/testdir2/test.txt"},
			want:      false,
		},
		"eptyValue": {
			firstVal:  []string{},
			secondVal: []string{"/home/opus/testdir2/", "/home/opus/testdir2/test.txt"},
			want:      false,
		},
		"invalidValie": {
			firstVal:  []string{"wefwefwef", "wef2332"},
			secondVal: []string{"/home/opus/testdir2/", "/home/opus/testdir2/test.txt"},
			want:      false,
		},
	}
	for name, testCase := range tests {
		t.Run(name, func(t *testing.T) {
			res, err := Equal(testCase.firstVal, testCase.secondVal)
			if err != nil {
				req.EqualError(err, "invalid Value")
			} else {
				req.NoError(err)
			}
			req.Equal(testCase.want, res)
		})
	}
}
