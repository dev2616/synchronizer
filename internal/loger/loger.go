package loger

import (
	"io"
	"log"
	"os"
)

type logChanel chan string

func (l logChanel) logAction(logFile io.Writer) {
	Info := log.New(logFile, "[INFO]\t", log.Ldate|log.Ltime)
	for {
		select {
		case msg := <-l:
			Info.Println(msg)
		}
	}
}

func NewLogChanel() logChanel {
	ch := make(logChanel)
	logFile, err := os.OpenFile("./log.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		return nil
	}
	go ch.logAction(logFile)
	return ch
}
