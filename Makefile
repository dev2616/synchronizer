build: 
	@go build -o sync ./cmd/app
test:
	@go test ./... -v
bench:
	@go test -bench=.  -benchmem  ./...
install:
	@./scripts/install.sh