package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"sync"

	"gitlab.rebrainme.com/golang_users_repos/2240/synchronizer/internal/observer"
)

func main() {
	srcDir := flag.String("sourceDir", "", "Example: /home/opus/dir")
	destDir := flag.String("destDir", "", "Example: : /home/opus/sync_dir")
	flag.Parse()

	// Инициализируем издателя
	pub := observer.NewPublisher()
	// Создаем WaitGroup для ожидания завершения издателя и подписчика
	var wg sync.WaitGroup
	// Инициализация контекста выхода издателя по сигналу
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()
	// Добавляем одну еденицу в WaitGroup
	wg.Add(1)
	// Запускаем издателя в отдельной горутине
	go pub.Start(ctx, &wg, *srcDir)
	// Добавляем подписчика
	subCh := pub.AddSubscriber()
	sub := observer.NewSubscriber(*srcDir, *destDir)
	subCh <- sub

	// Ждем выполнения горутины издателя
	wg.Wait()
}
